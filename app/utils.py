import os #NOQA
from datetime import date


def int2date(argdate=int):
    """If you have date (int), use this method to obtain a date object.

    Args:
        argdate (int): Date as a regular integer value (example: 20160618)

    Return:
        dateandtime.date: A date object which corresponds to the given value
                          `argdate`.
    """
    argdate = int(argdate)
    year = int(argdate / 10000)
    month = int((argdate % 10000) / 100)
    day = int(argdate % 100)

    return date(year, month, day)


def get_base_name(file_path):
    """Get base name (with extesion).

    Arg:
        file_path (str): File name with extesion.

    Return:
        (str): File name without extension.
    """
    return os.path.basename(file_path)


def create_connection_string(**kwargs):
    try:
        kwargs = kwargs['conn_string']
    except KeyError as e:
        ...
    user = kwargs['user']
    password = kwargs['password']
    host = kwargs['host']
    port = kwargs['port']
    dbname = kwargs['dbname']

    conn_string = f"""host={host} dbname={dbname} user={user} password={password} port={port}"""

    return conn_string


def read_list_txt_file(file_path: str):

    try:
        with open(file_path, 'r') as f:
            list_name = [l.split('#')[0].split()
                         for l in f if l.split('#')[0].split() != []]
            list_name = [n[0] for n in list_name]
            return list_name
    except FileExistsError as e:
        print(e)
