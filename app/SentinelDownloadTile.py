from os import mkdir
from os.path import exists, join
from pathlib import Path

import yaml
from sentinelsat import SentinelAPI

import ConnectionDB as C
import utils as u

# sys.argv[1]
FOLDER_NAME = '/mnt/BRUTA'

# sys.argv[2].lower() == 'true'
TO_DOWNLOAD = 'true'

# Open yaml
with open(Path("app/config/const.yaml"), 'r') as f:
        const = yaml.safe_load(f)

# Open Datahub parameters
data_hub = const['data_hub']
conn_string = const['db']
conn_string = u.create_connection_string(**conn_string)


def list_img2download(conn_string: str, schema: str, table: str):
    con = C.Connection(conn_string)
    query = (
        f"SELECT uuid FROM {schema}.{table} "
        "WHERE date_download_img IS NULL "
        "ORDER BY substring(title, 40 , 5), substring(title, 12 , 8);"
    )
    try:
        list_uuid = [i[0] for i in con.run_query(query)]
        return list_uuid
    except TypeError as error:
        print(error)


def get_title(conn_string: str, schema: str, table: str, uuid: str):
    con = C.Connection(conn_string)
    query = f"SELECT title FROM {schema}.{table} WHERE uuid = '{uuid}'"
    try:
        title = con.run_query(query)[0][0]
        return title
    except TypeError as error:
        print(error)


def path_output_folder(folder_name: str):
    """Path of folder where files will store.

    Arguments:
        folder_name {str} -- The name of folder where all zip files downloaded
                            from scihub will store.

    Returns:
        [str] -- Path of folder
    """
    dst_folder = folder_name
    if not exists(dst_folder):
        try:
            mkdir(dst_folder)
            return dst_folder
        except FileExistsError as e:
            print(e, f"Can't create destination directory {dst_folder}")
    else:
        return dst_folder


def is_file_in_folder(folder: str, file_name: str, file_extention: str):
    """Check if file exist in folder.

    Parameters
    ----------
    folder : str
        Folder where file be
    file_name : str
        File name
    file_extention : str
        File extension

    Returns
    -------
    [bool]
        If True file be in folder, False otherwise.
    """

    if exists(join(folder, file_name+file_extention+'.incomplete')):
        return False
    else:
        file_path = join(folder, file_name+file_extention)
        return exists(file_path)


def insert_date_hour_db(conn_string: str, schema: str, table: str, column: str,
                        uuid: str):
    """Update column date_dowload with YYYY/MM/DD and HH:MM:SSSS

    Parameters
    ----------
    conn_string : str
        conn_stromg --> host=localhost dbname=dbname user=user_db
                        password=password_db port=5432
    schema : str
        Schema name
    table : str
        Table name
    column : str
        Column name that is saved date and time when file is download.
    uuid : str
        Unique identifier
    """
    con = C.Connection(conn_string)
    query = (
        f"UPDATE {schema}.{table} "
        f"SET {column} = current_timestamp WHERE uuid = '{uuid}'"
    )
    con.run_update(query)


def dowload_img(list_index, dst_folder):
    # connect to the API
    api = SentinelAPI(data_hub['user'], data_hub['password'],
                      'https://scihub.copernicus.eu/dhus')
    for i in list_index:

        title = get_title(conn_string, schema='metadado_img',
                          table='metadado_sentinel', uuid=i)

        print(title)

        if TO_DOWNLOAD:
            try:
                product_info = api.get_product_odata(i)

                if product_info['Online']:
                    print(
                        f"Product {title} - {i} is online. Starting download."
                    )
                    api.download(i, directory_path=dst_folder)
                else:
                    ...

                file_already_download = is_file_in_folder(
                    folder=FOLDER_NAME, file_name=title, file_extention='.zip')

                if file_already_download:

                    insert_date_hour_db(conn_string=conn_string,
                                        schema='metadado_img',
                                        table='metadado_sentinel', uuid=i,
                                        column='date_download_img')
            except Exception as e:
                print(e)


dst_folder = path_output_folder(FOLDER_NAME)
list_index = list_img2download(conn_string,
                               'metadado_img', 'metadado_sentinel')
dowload_img(list_index, dst_folder)
