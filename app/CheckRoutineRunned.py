"Check if routine runned."
from datetime import date
from pathlib import Path


class CheckRoutineRunned:
    
    # The date of todau
    today_date = date.today()
    # Setting of file where we'll be write if routine was runned.
    file = Path("app/data/file/check_metadata_update.txt")
        
    def create_txt_file(self):
        """Create file where save the last date when application runned."""      
        try:
            file.touch()
            file.write_text(str(self.today_date))
        except FileNotFoundError as err:
            print(err)


    def read_date_in_txt_file(self):
        return self.file.read_text()

    
    def __call__(self):
        return self.read_date_in_txt_file() == self.today_date()

