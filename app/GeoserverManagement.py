import os
from pathlib import Path

import MonthDictionary as m
from geoserver.catalog import Catalog


def create_path_wms(file_name):
    month = file_name[13:15]
    month_name = get_mounth_name(month)
    sentinel_tile = file_name[3:8]
    year = file_name[9:13]
    if file_name[:2] == 'S2':
        return f'S2/{sentinel_tile}/{year}/{month_name}'


def get_mounth_name(month):
    """Do the name of month with cardinal and literal word of monthself."""
    string_mounth = m.month[int(month)]

    month_folder_name = '{}_{}'.format(month, string_mounth)

    return month_folder_name


class ListImageNotHasWMSService:

    def __init__(self, list_folder_path: list, geoserver_url: str,
                 list_file=None):
        """Processo to create wms service of image.

            1) Listar todos as imagens que podem ser cadastradas na pasta
            PROCESSADA
            2) Listas imagens que já foram cadastradas no geoserver
            3) Fazer a diferença entre essas listas para não cadastrar o que
            já foi feito

        Arguments:
            folder_path {[type]} -- [description]
            list_file {[type]} -- [description]
            geoserver_url {[type]} -- [description]
        """
        self.folder_path = list_folder_path
        self.geoserver_url = geoserver_url
        self.list_file = list_file

    def _list_files_to_create_wms_folder(self, extension='.TIF'):
        """Get list of file path which has extention

        Parameters
        ----------
        extension : str, optional
            The extention of file that search for in folder, by default '.TIF'

        Returns
        -------
        list
            List of file path from defined folder
        """

        """
        What is the sing of list_raster
        ll - list of list    [[   ll  ]  [  ll   ]]
        e = element of lista [[e, e, e], [e, e, e]]
        """

        list_raster = [e for ll in [
            list(Path(x).glob(f'**/*{extension}')) for x in self.folder_path
            ] for e in ll]
        return list_raster

    def get_file_info_as_dict(self):
        dict_files = {
            n.stem: n for n in self._list_files_to_create_wms_folder()}
        return dict_files

    def _get_all_file_name_from_folder_path(self):
        all_file_name = [
            n.stem for n in self._list_files_to_create_wms_folder()]
        return all_file_name

    def _list_all_layers_geoserver(self):
        """Get all layers name from geoserver

        Returns
        -------
        list
            List all layers name from geoserver
        """
        cat = Catalog(self.geoserver_url)
        all_layers = cat.get_layers()
        all_layers_list = [a.name[10:] for a in all_layers]
        return all_layers_list

    def list_img_not_has_wms_service(self):
        """Get all scene name which doesn't have WMS in goserver

        Returns
        -------
        list
            List with all scene name of files
        """
        if self.folder_path:
            img_list = [
                self.get_file_path_name_extension(x)
                for x in self._list_files_to_create_wms_folder()
                if self.get_file_path_name_extension(x)['stem']
                not in self._list_all_layers_geoserver()]
            return img_list
        else:
            pass

    @staticmethod
    def get_file_path_name_extension(file_path):
        """Get features from file path

        Parameters
        ----------
        file_path : str
            Absolute file path

        Returns
        -------
        dict
            Return dict from file with keys as:
                {'absolute_path': [value --> str],
                'relative_path': [value --> str],
                'name': [value --> str],
                'stem': [value --> str],
                'suffix': [value --> str],
                'relative_sld_path': [value --> str}
        """
        input_file = Path(file_path)
        absolute_path, relative_path, name, \
            stem, suffix, relative_sld_path, uri_path = str(input_file.absolute()), \
            str(input_file.relative_to('/mnt/')), \
            input_file.name, input_file.stem, input_file.suffix, \
            f"{str(input_file.relative_to('/mnt/').with_suffix(''))}.sld", \
            f"file://{str(input_file.absolute())}"

        d = dict(zip(('absolute_path', 'relative_path',  'name', 'stem',
                      'suffix', 'relative_sld_path', 'uri_path'),
                     (absolute_path, relative_path, name, stem, suffix,
                      relative_sld_path, uri_path)))
        return d


class CreateGeoserverWmsImage:
    def __init__(self, workspace: str, file_path: str, file_name: str,
                 sld_path: str, geoserver_url: str, username='admin',
                 password='geoserver'):
        # Geoserver features
        self.username = username
        self.password = password
        self.geoserver_url = geoserver_url
        self.workspace = workspace
        self.sld_path = sld_path

        self.cat = Catalog(self.geoserver_url, self.username, self.password)

        # File features
        self.file_path = file_path
        self.file_name = file_name

        self.store_name = f'{self.workspace}:{self.file_name}'
        self.layer_name = f'{self.workspace}:{self.file_name}'

    def load_geotiff(self):
        try:
            self.cat.create_coveragestore(
                name=self.store_name, layer_name=self.file_name,
                workspace=self.workspace, path=self.file_path)
        except IndexError as ex:
            print('Error: load geotiff:', ex)
            pass

    def load_sld(self):
        with open(self.sld_path) as f:
            self.cat.create_style(name=self.file_name, data=f.read(),
                workspace=self.workspace, overwrite=True)
        

    def set_style_to_layer(self):
        try:
            layer = self.cat.get_layer(self.layer_name)
            layer.default_style = self.layer_name
            self.cat.save(layer)
        except Exception as e:
            print('Error: set style to layer: ', e)

    def set_abstract_layer(self):
        try:
            resource = self.cat.get_resource(name=self.file_name, 
                                             workspace=self.workspace)
            resource.abstract = self.file_name
            self.cat.save(resource)
        except Exception as e:
            print('Error: set abstract layer: ', e)

    def set_input_transparent_color_value(self):
        """
          exemple:
          'curl -u admin:geoserver -XPUT -H  "accept: text/html"
          -H  "content-type: application/xml"
          -d "<coverage><parameters><entry><string>
          InputTransparentColor</string><string>#000000</string></entry>
          </parameters></coverage>"
          http://172.18.0.2:8080/geoserver/rest/workspaces/sentinel2/
          coveragestores/sentinel2:S2_24LUH_20190829/coverages/S2_24LUH_20190829.xml'
        """

        command = f"""curl -u admin:geoserver -XPUT -H  "accept: text/html" -H "content-type: application/xml" -d "<coverage><parameters><entry><string>InputTransparentColor</string><string>#000000</string></entry></parameters></coverage>" {self.geoserver_url}/workspaces/{self.workspace}/coveragestores/{self.store_name}/coverages/{self.file_name}.xml"""
        os.system(command)

    def set_path_wms(self):
        """curl -u admin:ge-XPUT -H \"Content-Type: text/xml"
           -d "<layer><path>S2/2018/06_Junho</path></layer>"
           http://geoserver-homo.inema.ba.gov.br/geoserver/rest/layers/sentinel2:S2_24LUH_20180608.xml
        """
        str_path_wms = create_path_wms(self.file_name)
        # storename = self.layer_name
        command = f"""curl -u {self.username}:{self.password} -XPUT -H  "accept: text/html" -H  "content-type: application/xml" -d "<layer><path>{str_path_wms}</path></layer>" {self.geoserver_url}/layers/{self.file_name}.xml"""
        os.system(command)

    def del_store(self):
        store_name = self.store_name
        layer = self.cat.get_layer(store_name)
        layer.enabled=True
        self.cat.save(layer)
        self.cat.reload()

        st = self.cat.get_store(store_name, workspace=self.workspace)
        self.cat.delete(layer)
        self.cat.reload()
        self.cat.delete(st)
        self.cat.reload()

    def register_img_geoserver(self):
        try:
            self.load_geotiff()
            self.load_sld()
            self.set_style_to_layer()
            # self.set_abstract_layer()
            self.set_input_transparent_color_value()
            self.set_path_wms()
        except:
            self.del_store()
       

def main(geoserver_url: str, list_folder_path: list, username=None,
         password=None, list_file=None):

    list_dict_img_not_has_wms = ListImageNotHasWMSService(
        list_folder_path,  geoserver_url,
        list_file).list_img_not_has_wms_service()

    for img in list_dict_img_not_has_wms:

        relative_tif_path = img['relative_path']
        file_name = img['stem']
        relative_sld_path = img['relative_sld_path']

        folder_tif_path_geoserver = '/root/GEODADOS/CANDEIAS/'
        file_path = f"{folder_tif_path_geoserver}{relative_tif_path}"
        # Lugar onde a pasta PROCESSADA esta na máquina deste código esta
        # rodando
        root_folder = '/mnt/'
        sld_path = f"{root_folder}{relative_sld_path}"


        CreateGeoserverWmsImage(workspace='sentinel2', file_path=file_path,
                                file_name=file_name, sld_path=sld_path,
                                geoserver_url=geoserver_url, username=username,
                                password=password).register_img_geoserver()


if __name__ == "__main__":

    # Local onde estão os arquivos das imagens do host
    # que esta rodando o código
    list_folder_path = [
                        '/mnt/PROCESSADA/S2/24LWN/2020/11_Novembro/',
                        '/mnt/PROCESSADA/S2/24LWM/2020/11_Novembro/',
                        '/mnt/PROCESSADA/S2/24LWL/2020/11_Novembro/',
                        '/mnt/PROCESSADA/S2/24LXM/2020/11_Novembro/',
                        '/mnt/PROCESSADA/S2/24LXN/2020/11_Novembro/'
                        #
                        # '/mnt/PROCESSADA/S2/24LVM/2019/',
                        # '/mnt/PROCESSADA/S2/24LVM/2017/',
                        # '/mnt/PROCESSADA/S2/24LVM/2015/',
                        # '/mnt/PROCESSADA/S2/24LVL/2018/',
                        # '/mnt/PROCESSADA/S2/24LVL/2017/',
                        #'/mnt/PROCESSADA/S2/24LVL/2019/'
                        # '/mnt/PROCESSADA/S2/24LVK/2017/',
                        # '/mnt/PROCESSADA/S2/24LVJ/2020/'
                        # '/mnt/PROCESSADA/S2/24LVH/2017/'
                        #
                        # '/mnt/PROCESSADA/S2/24LUL/2016/',
                        # '/mnt/PROCESSADA/S2/24LUK/2016/',
                        # '/mnt/PROCESSADA/S2/24LUK/2018/'
                        # '/mnt/PROCESSADA/S2/24LUJ/2016/',
                        # '/mnt/PROCESSADA/S2/24LUJ/2020/'
                        # '/mnt/PROCESSADA/S2/24LUH/2016/',
                        # '/mnt/PROCESSADA/S2/24LUH/2017/'
                        #
                        # '/mnt/PROCESSADA/S2/24KUG/',
                        # '/mnt/PROCESSADA/S2/24KUF/',
                        # '/mnt/PROCESSADA/S2/24KUE/',
                        # '/mnt/PROCESSADA/S2/24KVG/',
                        # '/mnt/PROCESSADA/S2/24KVE/',
                        # '/mnt/PROCESSADA/S2/24KVF/'
                        ]

    geoserver_url_local = 'http://geoserver-homo.inema.ba.gov.br/rest'
    # geoserver_url_local = 'http://http://localhost:80/rest'

    main(list_folder_path=list_folder_path, geoserver_url=geoserver_url_local,
         username='admin', password='geoserver')
