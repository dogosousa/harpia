import sys
from datetime import date
from datetime import datetime as dt
from pathlib import Path

import pandas as pd
import yaml
from geopandas_postgis import PostGIS  # NOQA
from sentinelsat import SentinelAPI, geojson_to_wkt, read_geojson
from sqlalchemy import create_engine

import ConnectionDB as C
import utils as u

# Open yaml
with open(Path("app/config/const.yaml"), 'r') as f:
    const = yaml.safe_load(f)

# Read const parameters
data_hub = const['data_hub']
conn_string = const['db']
aoi_file_path = const['aoi_download']['path']
dates = const['date_download']['initial_end']
cloud_cover_past = const['cloud_cover']['min_max_past']
cloud_cover_present = const['cloud_cover']['min_max_present']
sentinel_txt_path = const['list_sentinel_name']['txt_path']

# connect to the API
api = SentinelAPI(data_hub['user'], data_hub['password'],
                  'https://scihub.copernicus.eu/dhus')


def as_footprint(aoi_file_path):
    path = Path(aoi_file_path)
    if path.exists():
        return path
    else:
        print('Set area of interest to download in app/config/const.yaml file')
        sys.exit()


def get_sentinel_metadata(date: tuple, platformname: str, producttype: str,
                          cloud_cover: tuple):

    footprint = geojson_to_wkt(read_geojson(as_footprint(aoi_file_path)))
    try:
        if not request_metadata_already_done():
            products = api.query(
                footprint, date=date, platformname=platformname,
                producttype=producttype, cloudcoverpercentage=cloud_cover)
            gdf = api.to_geodataframe(products)

            return gdf
    except Exception:
        print('Request metadata have already done today')


def get_sentinel_metadata_past(dates, cloud_cover=cloud_cover_past):
    # When the project will begin
    initial_date = dates[0]
    # When the project will end
    end_date = dates[1]

    gdfL2A = get_sentinel_metadata(date=('20181231', end_date),
                                   platformname='Sentinel-2',
                                   producttype='S2MSI2A',
                                   cloud_cover=cloud_cover)

    if dt.strptime(initial_date, "%Y%m%d") < dt.strptime('20181231', "%Y%m%d"):

        gdfL1C = get_sentinel_metadata(date=(initial_date, '20181231'),
                                       platformname='Sentinel-2',
                                       producttype='S2MSI1C',
                                       cloud_cover=cloud_cover)

        try:
            gdf = pd.concat([gdfL1C, gdfL2A], sort=True, ignore_index=True)
            return gdf
        except Exception as e:
            print("There aren\'t metadata to request.", e)

    else:
        return gdfL2A


def get_sentinel_metadata_present(cloud_cover=cloud_cover_present):
    """Get 30 days from the present images"""
    gdf = get_sentinel_metadata(date=('NOW-30DAYS', 'NOW'),
                                platformname='Sentinel-2',
                                producttype='S2MSI2A',
                                cloud_cover=cloud_cover)
    return gdf


def get_sentinel_metadata_by_names(names):
    """Get metadata by list of name."""
    products = api._query_names(names)

    d = {list(i.keys())[0]: list(i.values())[0] for i in
         products.values()}

    gdf_products = api.to_geodataframe(d)
    return gdf_products


def metadata_img_is_saved_db(schema: str, table: str, uuid: str, **kwargs):
    """Check is metadado from satellite image was saved in postgres database.

    Arguments:
        conn_string {str} -- String to connect to postgres database
             conn_stromg --> host=localhost dbname=dbname user=user_db
                password=password_db port=5432
        schema {str} -- shcema name
        table {str} -- table name from schema
        uuid {str} -- single identification of sentinel satellite 2 image

    Returns:
        [bool] -- The return value. True if file has metadata saved in database
                table, False otherwise.
    """
    conn_string = u.create_connection_string(**kwargs)
    # Connect to Database
    con = C.Connection(conn_string)
    query = f"SELECT uuid FROM {schema}.{table} WHERE uuid = '{uuid}'"
    try:
        metadado_was_saved_db = (len(con.run_query(query)) >= 1)
        return metadado_was_saved_db
    except TypeError as error:
        print(error)


def load_sentinel_metadata_db(gdf, engine):

    for i in range(0, len(gdf)):

        uuid = gdf['uuid'][i]

        # Check if file was downloaded anytime
        metadata_save_db = metadata_img_is_saved_db(
            conn_string=conn_string, schema='metadado_img',
            table='metadado_sentinel', uuid=uuid)

        if not metadata_save_db:
            # Select row from dataframe
            g = gdf[gdf['uuid'] == uuid].copy()
            g.postgis.to_postgis(con=engine, schema='metadado_img',
                                 if_exists='append',
                                 table_name='metadado_sentinel',
                                 geometry='Geometry')


def create_sqlalchemy_engine(**kwargs):
    # Create engine to use with sqlalchemy
    user = kwargs['user']
    password = kwargs['password']
    host = kwargs['host']
    port = kwargs['port']
    dbname = kwargs['dbname']
    engine_con = f'postgresql://{user}:{password}@{host}:{port}/{dbname}'
    engine = create_engine(engine_con)
    return engine


def create_file_check_update_metadata(
        file_path="app/data/file/check_metadata_update.txt"):
    """Create file where save the last date when application request
    metadata."""

    today = date.today()

    with open(file_path, 'w') as f:
        f.write(str(today))

    return file_path


def request_metadata_already_done(
        file_path="app/data/file/check_metadata_update.txt"):
    today = date.today()

    with open(file_path, 'r') as f:
        day = f.read()

    request_metadata_have_done = (day == str(today))

    return request_metadata_have_done


# Create engine
engine = create_sqlalchemy_engine(**conn_string)

# Get past and present sentinel metadata
past = get_sentinel_metadata_past(dates=dates)
present = get_sentinel_metadata_present(cloud_cover=cloud_cover_present)

create_file_check_update_metadata()

# Get sentinel metadata from list of tile's name
list_file_name = u.read_list_txt_file(sentinel_txt_path)
from_list_name = get_sentinel_metadata_by_names(list_file_name)

# Load in postgres database
gdf = pd.concat([past, present, from_list_name], sort=True, ignore_index=True)
load_sentinel_metadata_db(gdf, engine)
