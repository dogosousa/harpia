FROM python:3.7-stretch

# Update base container install
RUN apt-get update
RUN apt-get upgrade -y

# Add unstable repo to allow us to access latest GDAL builds
RUN echo deb http://ftp.br.debian.org/debian unstable main contrib non-free >> /etc/apt/sources.list
RUN apt-get update

## Existing binutils causes a dependency conflict, correct version will be installed when GDAL gets intalled
RUN apt-get remove -y binutils
#
## Install GDAL dependencies
RUN apt-get install -y libgdal-dev g++ gdal-bin
#
## Update C env vars so compiler can find gdal
ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal
ENV PROJ_LIB=/usr/share/proj/

RUN pip install --upgrade pip 

#
## This will install latest version of GDAL
RUN pip install GDAL==3.0.4

RUN pip install git+https://github.com/ubarsc/rios@rios-1.4.8
RUN pip install git+https://github.com/ubarsc/python-fmask@pythonfmask-0.5.4
RUN pip install git+https://github.com/GeoNode/geoserver-restconfig@1.0.6


# Copy project dependencies code in Docker
ADD requirements.txt /requirements.txt
RUN pip install --trusted-host pypi.python.org -r /requirements.txt
RUN rm /requirements.txt

WORKDIR /workspace/harpia

ADD run.sh /workspace/harpia/run.sh
RUN chmod +x /workspace/harpia/run.sh

CMD ["./run.sh"]
