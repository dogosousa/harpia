from datetime import date
from pathlib import Path

from app.CheckRoutineRunned import CheckRoutineRunned as crr

    
today_date = date.today()

def test_file_path():
    assert crr().file.__str__() == "app/data/file/check_metadata_update.txt"


def test_create_txt_file():
    """Create txt file and wrint today date into it.
    """    
    assert crr().file.exists()
 

def test_read_date_in_txt_file():
    """Read txt file and see if there is today date into it.
    """    
    assert crr().read_date_in_txt_file() == str(today_date)


def test_code_runned_today():
    """Chech if routine was runned today.
    """
    assert crr()
