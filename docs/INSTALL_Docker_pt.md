# Criando imagem docker - Harpia

#### Construindo a imagem docker onde rodar o Harpia

```bash
docker build --rm -t harpia:0.5 .
```
#
### Executando a aplicação

\
Para execução da aplicação será necessário a implementação dos volumes que vinculam os diretores do docker ao host. Atentar para esses parametros de modo que a aplicação funcione corretamente.

Os parametros abaixo são aqueles que indicam onde o docker encontrará os arquivos necessário para sua execução. Os volumes indicam onde os arquivos brutos serão armazenados, onde os arquivos processados serão salvos, qual a pasta que o .txt que indica a lista de arquivos para processar que não estejam no critério da área de interesse (aoi) e onde esta o codigo que será executado.

Estrutura dos volumes:   **{host_folder}:{docker_folder}** 

- Volume que indica onde serão salvos os dados brutos no host (ex. sentinel .zip dos arquivos .SAFE) \
*-v "/media/diogo/D1/:/mnt/BRUTA/"*


- Local onde os arquivos processados serão salvos
*-v "/home/diogo/Documents/HARPIA/PROCESSADA:/mnt/PROCESSADA/"*


- Volume onde os arquivo .txt da lista dos sentinel para serem baixados fora do aoi
-v "/mnt/ipira/PUBLICA_COTIC/HARPIA/:/mnt/PUBLICA_COTIC/" 

- Volume onde esta o diretorio com o arquivo de configuração da codigo do harpia robô
*-v "/home/diogo/workspace/harpia/app/config/:/workspace/harpia/app/config/"*

- Volume onde esta o diretório com dados necessarios para execução do harpia robô
-v "/home/diogo/workspace/harpia/app/data/:/workspace/harpia/app/data/"



\
Executando a aplicação para os parametros definidos acima

```bash
docker run -v "/media/diogo/D1/:/mnt/BRUTA/" -v "/home/diogo/Documents/HARPIA/PROCESSADA:/mnt/PROCESSADA/" -v "/mnt/ipira/PUBLICA_COTIC/HARPIA/:/mnt/PUBLICA_COTIC/" -v "/home/diogo/workspace/harpia/app/config/:/workspace/harpia/app/config/" -v "/home/diogo/workspace/harpia/app/data/:/workspace/harpia/app/data/" --rm --network host --name harpia harpia:0.5
```

