# Create image from Dockerfile

```bash
docker build --rm -t harpia:0.5 .

docker run -v "/media/diogo/D1/:/mnt/BRUTA/" -v "/home/diogo/Documents/HARPIA/PROCESSADA:/mnt/PROCESSADA/" -v "/mnt/ipira/PUBLICA_COTIC/HARPIA/:/mnt/PUBLICA_COTIC/" -v "/home/diogo/workspace/harpia/app/config/:/workspace/harpia/app/config/" -v "/home/diogo/workspace/harpia/app/data/:/workspace/harpia/app/data/" --rm --network host --name harpia harpia:0.5
```

